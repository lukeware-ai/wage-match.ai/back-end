## create project
```shell
python3 -m venv ./.venv
```

```shell
source .venv/bin/activate
```

## install as dependency
```shell
pip install -r ./requirements.txt
```

## Liberar porta maquina local
```shell
sudo ufw allow 8881/tcp
```