from datetime import datetime
import os
import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error, r2_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder, StandardScaler
import joblib

from generate_ai import GenerateAI
from validation import Validate


FILENAME_CSV = os.path.join("csv", "learning_offers_for_candidates.csv")
FILENAME_MODEL = os.path.join("model", "gradient_boosting_regressor_model.pkl")


class ModeloService:

    @classmethod
    def predict(cls, data: dict) -> dict:
        Validate.valid_dict(data, Validate.REQUIRED_FIELDS_PREDICT_DEFAULT)

        df = pd.DataFrame({key: [value] for key, value in data.items()})

        model: Pipeline = joblib.load(FILENAME_MODEL)
        predict_value = model.predict(df)

        reps = {"label": float(predict_value[0])}

        new_insts, is_new = Validate.is_new_instance(data, filename=FILENAME_CSV)
        if is_new:
            reps["new_instance"] = new_insts[0]

        return reps

    @classmethod
    def add_instance(cls, data: dict) -> None:
        Validate.valid_dict(data, Validate.REQUIRED_FIELDS_DEFAULT)
        GenerateAI.new_workout(data, FILENAME_MODEL, filename_csv=FILENAME_CSV)

    @classmethod
    def generate(cls) -> dict:
        return GenerateAI.generate(FILENAME_CSV, FILENAME_MODEL)

    @classmethod
    def update_model(cls) -> None:
        df = pd.read_csv(FILENAME_CSV)

        X = df.drop(columns=["oferta_final", "tempo_experiencia"], errors="ignore")
        y = df["oferta_final"]

        numeric_features = X.select_dtypes(include=["int64", "float64"]).columns
        categorical_features = X.select_dtypes(include=["object"]).columns

        preprocessor = ColumnTransformer(
            transformers=[
                ("num", StandardScaler(), numeric_features),
                ("cat", OneHotEncoder(handle_unknown="ignore"), categorical_features),
            ]
        )

        model = Pipeline(
            steps=[
                ("preprocessor", preprocessor),
                (
                    "regressor",
                    GradientBoostingRegressor(
                        learning_rate=0.1, max_depth=5, n_estimators=200
                    ),
                ),
            ]
        )

        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.2, random_state=42
        )

        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)

        r2 = r2_score(y_test, y_pred)
        print(f"score r2: {r2}")

        mae = mean_absolute_error(y_test, y_pred)
        print(f"Mean Absolute Error: {mae:.2f}")

        if r2 > 0.85:
            Validate.valid_directory_exists("model")
            joblib.dump(model, FILENAME_MODEL)
        else:
            raise Exception("Very low score")
