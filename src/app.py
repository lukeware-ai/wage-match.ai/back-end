from http import HTTPStatus
import logging
from operator import imod
import os
from flask import Flask, jsonify
from mode_controller import model_controller
from flask_cors import CORS

VERSION_API = "api/v1"

app = Flask(__name__)
CORS(app)

app.register_blueprint(model_controller, url_prefix=f"/wage-match-ai/{VERSION_API}/")


if __name__ != "__main__":
    gunicorn_logger = logging.getLogger("gunicorn.error")
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)


@app.errorhandler(ValueError)
def handle_value_error(e):
    return jsonify({"error": str(e)}), HTTPStatus.BAD_REQUEST


@app.errorhandler(Exception)
def handle_exception_error(e):
    return jsonify({"error": str(e)}), HTTPStatus.BAD_REQUEST


# Execute o aplicativo Flask
if __name__ == "__main__":
    port = int(os.environ.get("PORT", 8890))
    app.run(host='0.0.0.0', port=port, debug=True)
