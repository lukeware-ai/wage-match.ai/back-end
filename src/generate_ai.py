from datetime import datetime
import os
import joblib
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error, r2_score
from sklearn.pipeline import Pipeline

from validation import Validate

AMOUNT_INSTANCES = 1000
FORMAT = "%Y%m%d%H%M%S"
FILENAME_MODEL_FAVORITE = os.path.join(
    "model", f"gradient_boosting_regressor_model_{datetime.now().strftime(FORMAT)}.pkl"
)


class GenerateAI:
    @classmethod
    def new_workout(cls, data: dict, filename_model: str, filename_csv: str) -> None:
        csv_df = pd.read_csv(filename_csv)
        new_data = pd.DataFrame({key: [value] * AMOUNT_INSTANCES for key, value in data.items()})

        df = pd.concat([csv_df, new_data], ignore_index=True)
        df["oferta_final"] = df["oferta_final"].round(2)
        # df = df.drop_duplicates()
        df = df.sort_values(by="oferta_final")
        df.to_csv(filename_csv, index=False)

        X = df.drop(columns=["oferta_final", "tempo_experiencia"], errors="ignore")
        y = df["oferta_final"]

        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.2, random_state=42
        )

        model: Pipeline = joblib.load(filename_model)
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)

        r2 = r2_score(y_test, y_pred)
        print(f"score r2: {r2}")
        mae = mean_absolute_error(y_test, y_pred)
        print(f"Mean Absolute Error: {mae:.2f}")

        joblib.dump(model, filename_model)

    @classmethod
    def generate(cls, filename_csv: str, fileaname_model: str):
        df = pd.read_csv(filename_csv)

        new_data = GenerateAI._create_new_df(df)

        model: Pipeline = joblib.load(fileaname_model)
        new_data["oferta_final"] = model.predict(new_data)

        df = pd.concat([df, new_data], ignore_index=True)
        df["oferta_final"] = df["oferta_final"].round(2)
        # df = df.drop_duplicates()
        df = df.sort_values(by="oferta_final")
        df.to_csv(filename_csv, index=False)

        X = df.drop(columns=["oferta_final", "tempo_experiencia"], errors="ignore")
        y = df["oferta_final"]

        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.2, random_state=42
        )
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)

        r2 = r2_score(y_test, y_pred)
        print(f"score r2: {r2}")

        mae = mean_absolute_error(y_test, y_pred)
        print(f"Mean Absolute Error: {mae:.2f}")
        resp = {}
        if r2 > 0.80:
            Validate.valid_directory_exists("model")
            joblib.dump(model, fileaname_model)
            resp["score"] = f"{r2:.5f}"
            resp["mean"] = f"{mae:.2f}"
        else:
            raise Exception("very low score")

        if r2 >= 0.999:
            joblib.dump(model, FILENAME_MODEL_FAVORITE)

        return resp

    @classmethod
    def _create_new_df(cls, df: pd.DataFrame):

        new_data_sintect = {
            "ano_atual": [2023],
            "area": np.unique(df["area"].values),
            "cargo": np.unique(df["cargo"].values),
            "estado": np.unique(df["estado"].values),
            "ex_liguagem_desenv": np.unique(df["ex_liguagem_desenv"].values),
            "nivel": np.arange(df["nivel"].min(), df["nivel"].max() + 1, 1),
            "demanda": np.arange(df["demanda"].min(), df["demanda"].max() + 1, 1),
            "ex_framework_1_0": [0, 1],
            "ex_outros_frameworks_1_0": [0, 1],
            "ex_idea_1_0": [0, 1],
            "ex_outras_ideas_1_0": [0, 1],
            "ex_git_1_0": [0, 1],
            "ex_jira_1_0": [0, 1],
            "ex_confluence_1_0": [0, 1],
            "ex_gitlab_1_0": [0, 1],
            "ex_github_1_0": [0, 1],
            "ex_azure_devops_1_0": [0, 1],
            "ex_ci_cd_1_0": [0, 1],
            "ex_cloud_1_0": [0, 1],
            "ex_outras_clouds_1_0": [0, 1],
            "ex_database_1_0": [0, 1],
            "ex_outros_database_1_0": [0, 1],
            "ex_tdd_1_0": [0, 1],
            "ex_unit_test_1_0": [0, 1],
            "ex_integration_test_1_0": [0, 1],
            "ex_scrum_1_0": [0, 1],
            "ex_kanban_1_0": [0, 1],
            "atuou_projeto_ai_1_0": [0, 1],
            "domina_ai_1_0": [0, 1],
            "regime_contratacao_1_0": [0, 1],
        }

        new_datas = {}
        for colunms, valores in new_data_sintect.items():
            new_datas[colunms] = np.random.choice(valores, size=AMOUNT_INSTANCES)

        return pd.DataFrame(new_datas)
