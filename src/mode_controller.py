import csv
from http import HTTPStatus
from flask import Blueprint, jsonify, request
import pandas as pd

from model_service import ModeloService

model_controller = Blueprint("model", __name__)

@model_controller.route("/predict", methods=["POST"])
def predict():
    data = ModeloService.predict(request.get_json(force=True))
    return jsonify({"status": "success", "data": data}), HTTPStatus.OK


@model_controller.route("/instance", methods=["PUT"])
def add():
    ModeloService.add_instance(request.get_json(force=True))
    return jsonify({"status": "success"}), HTTPStatus.OK


@model_controller.route("/pre-workout", methods=["PUT"])
def update_model():
    ModeloService.update_model()
    return jsonify({"status": "success"}), HTTPStatus.OK


@model_controller.route("/generate", methods=["PUT"])
def generate():
    data = ModeloService.generate()
    return jsonify({"status": "success", "data": data}), HTTPStatus.OK


@model_controller.route("/health", methods=["GET"])
def health():
    return jsonify({"status": "success"}), HTTPStatus.OK
