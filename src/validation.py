import os

import pandas as pd


class Validate:

    REQUIRED_FIELDS_PREDICT_DEFAULT = {
        "ano_atual": lambda x: isinstance(x, int) and x > 1924,
        "area": lambda x: isinstance(x, str) and len(x) > 0,
        "cargo": lambda x: isinstance(x, str) and len(x) > 0,
        "estado": lambda x: isinstance(x, str) and len(x) > 0,
        "ex_liguagem_desenv": lambda x: isinstance(x, str) and len(x) > 0,
        "nivel": lambda x: isinstance(x, (int, float)) and x >= 0,
        "demanda": lambda x: isinstance(x, (int, float)) and x >= 0,
        "ex_framework_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_outros_frameworks_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_idea_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_outras_ideas_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_git_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_jira_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_confluence_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_gitlab_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_github_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_azure_devops_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_ci_cd_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_cloud_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_outras_clouds_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_database_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_outros_database_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_tdd_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_unit_test_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_integration_test_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_scrum_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "ex_kanban_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "atuou_projeto_ai_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "domina_ai_1_0": lambda x: isinstance(x, int) and x in (0, 1),
        "regime_contratacao_1_0": lambda x: isinstance(x, int) and x in (0, 1),
    }

    REQUIRED_FIELDS_DEFAULT = {
        **REQUIRED_FIELDS_PREDICT_DEFAULT,
        "oferta_final": lambda x: isinstance(x, (int, float)) and x > 0,
    }

    @classmethod
    def valid_directory_exists(cls, directory):
        if not os.path.exists(directory):
            os.makedirs(directory)

    @classmethod
    def is_new_instance(cls, instance: dict, filename: str) -> list[dict]:
        data = instance.copy()
        df = pd.read_csv(filename)
        new_inst = pd.DataFrame({key: [value] for key, value in data.items()})
        new_inst_known = pd.DataFrame(columns=new_inst.columns)
        new_inst_unknown = pd.DataFrame(columns=new_inst.columns)

        def _is_known(row):
            return all(
                row[column] in df[column].unique()
                for column in new_inst.columns
                if column in df.columns
            )

        for _, row in new_inst.iterrows():
            if _is_known(row):
                new_inst_known = pd.concat(
                    [new_inst_known, pd.DataFrame([row])], ignore_index=True
                )
            else:
                new_inst_unknown = pd.concat(
                    [new_inst_unknown, pd.DataFrame([row])], ignore_index=True
                )

        if len(new_inst_unknown.head()) > 0:
            return new_inst_unknown.to_dict(orient="records"), True

        return None, False

    @classmethod
    def valid_dict(
        cls,
        data: dict,
        required_fields: dict = REQUIRED_FIELDS_DEFAULT,
        custom_messages: dict = None,
    ) -> None:
        default_messages = {
            "missing_field": "The required field '{field}' is missing.",
            "type_error": "The field '{field}' must be of type {expected_type}.",
            "value_error": "The value of '{field}' must be {expected_value}.",
            "validation_error": "The field '{field}' failed validation.",
        }
        custom_messages = custom_messages or {}

        for field, field_type in required_fields.items():
            field_value = data.get(field)
            if field_value is None:
                raise ValueError(default_messages["missing_field"].format(field=field))

            if isinstance(field_type, tuple):
                expected_type, expected_value = field_type
                message_key = field if field in custom_messages else "validation_error"
                message = custom_messages.get(
                    message_key, default_messages[message_key]
                )

                if not isinstance(field_value, expected_type):
                    raise TypeError(
                        default_messages["type_error"].format(
                            field=field, expected_type=expected_type.__name__
                        )
                    )
                if (
                    isinstance(expected_value, tuple)
                    and field_value not in expected_value
                ):
                    raise ValueError(
                        default_messages["value_error"].format(
                            field=field, expected_value=expected_value
                        )
                    )
                elif field_value != expected_value:
                    raise ValueError(
                        default_messages["value_error"].format(
                            field=field, expected_value=expected_value
                        )
                    )
            else:  # Se for uma função (lambda)
                if not field_type(field_value):
                    message = custom_messages.get(
                        field, default_messages["validation_error"]
                    )
                    raise ValueError(message.format(field=field))
