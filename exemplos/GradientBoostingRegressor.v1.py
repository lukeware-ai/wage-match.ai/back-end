from codecs import ignore_errors
import joblib
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.ensemble import GradientBoostingRegressor  # Alteração 1
from sklearn.metrics import mean_absolute_error, r2_score
import plotly.graph_objs as go


def generate_new_data():
    # Defina as categorias para cada característica
    novos_dados_sinteticos = {
        "ano_atual": [2023],
        "area": ["desenvolvimento_de_software"],
        "cargo": ["desenvolvedor_back_end"],
        "estado": ["sao_paulo"],
        "ex_liguagem_desenv": ["python"],
        "nivel": np.arange(0, 2, 0.5),
        "demanda": np.arange(0, 2, 0.5),
        "ex_framework_1_0": [0,1],
        "ex_outros_frameworks_1_0": [0,1],
        "ex_idea_1_0": [0,1],
        "ex_outras_ideas_1_0": [0,1],
        "ex_git_1_0": [0,1],
        "ex_jira_1_0": [0,1],
        "ex_confluence_1_0": [0,1],
        "ex_gitlab_1_0": [0,1],
        "ex_github_1_0": [0,1],
        "ex_azure_devops_1_0": [0,1],
        "ex_ci_cd_1_0": [0,1],
        "ex_cloud_1_0": [0,1],
        "ex_outras_clouds_1_0": [0,1],
        "ex_database_1_0": [0,1],
        "ex_outros_database_1_0": [0,1],
        "ex_tdd_1_0": [0,1],
        "ex_unit_test_1_0": [0,1],
        "ex_integration_test_1_0": [0,1],
        "ex_scrum_1_0": [0,1],
        "ex_kanban_1_0": [0,1],
        "atuou_projeto_ai_1_0": [0,1],
        "domina_ai_1_0": [0,1],
        "regime_contratacao_1_0": [0,1],
    }

    # Defina a quantidade de dados sintéticos que você deseja gerar
    quantidade_dados = 10000

    # Gerar os dados sintéticos
    novos_dados = {}

    for coluna, valores in novos_dados_sinteticos.items():
        novos_dados[coluna] = np.random.choice(valores, size=quantidade_dados)

    # Retorne os novos dados sintéticos
    return pd.DataFrame(novos_dados)


# Carregar os dados do CSV
df = pd.read_csv("exemplos/oferta-back-end-python.csv", index_col=None)

# Separar as features (X) e o target (y)
X = df.drop(columns=["oferta_final", "tempo_experiencia"], errors="ignore")
y = df["oferta_final"]

# Identificar colunas numéricas e categóricas
numeric_features = X.select_dtypes(include=["int64", "float64"]).columns
categorical_features = X.select_dtypes(include=["object"]).columns

# Criar pré-processador para colunas numéricas e categóricas
preprocessor = ColumnTransformer(
    transformers=[
        ("num", StandardScaler(), numeric_features),
        ("cat", OneHotEncoder(handle_unknown="ignore"), categorical_features),
    ]
)

# Criar pipeline com pré-processador e modelo
model = Pipeline(
    steps=[
        ("preprocessor", preprocessor),
        (
            "regressor",
            GradientBoostingRegressor(learning_rate=0.1, max_depth=5, n_estimators=200),
        ),
    ]
)

# Dividir os dados em treino e teste
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.3, random_state=42
)

# Treinar o modelo
model.fit(X_train, y_train)

# Fazer previsões
y_pred = model.predict(X_test)

# Calcular o coeficiente de determinação (R²)
r2 = r2_score(y_test, y_pred)
print(f"score r2: {r2}")

# Avaliar o modelo
mae = mean_absolute_error(y_test, y_pred)
print(f"Mean Absolute Error: {mae:.2f}")


# -------------------------------------------------------------------------
# Fazer previsões nos novos dados sintéticos
novos_dados_AI_generate = generate_new_data()  # Suponha que você tenha uma função chamada generate_new_data() que gera os novos dados

X = novos_dados_AI_generate.drop_duplicates()
y = model.predict(novos_dados_AI_generate)  # Suponha que você tenha um modelo chamado model


model.fit(X, y)

y_pred = model.predict(X_test)

# Calcular o coeficiente de determinação (R²)
r2 = r2_score(y_test, y_pred)
print(f"score r2: {r2}")
# Avaliar o modelo
mae = mean_absolute_error(y_test, y_pred)
print(f"Mean Absolute Error: {mae:.2f}")

joblib.dump(model, "exemplos/modelo-v7.pkl")
# -------------------------------------------------------------------------


# Fazer previsões com novos dados (exemplo)

new_candidate = pd.DataFrame(
    {
        "ano_atual": [2023],
        "area": ["desenvolvimento_de_software"],
        "cargo": ["desenvolvedor_back_end"],
        "estado": ["sao_paulo"],
        "ex_liguagem_desenv": ["python"],
        "nivel": [2],
        "demanda": [0],
        "ex_framework_1_0": [0],
        "ex_outros_frameworks_1_0": [0],
        "ex_idea_1_0": [0],
        "ex_outras_ideas_1_0": [0],
        "ex_git_1_0": [0],
        "ex_jira_1_0": [0],
        "ex_confluence_1_0": [0],
        "ex_gitlab_1_0": [0],
        "ex_github_1_0": [0],
        "ex_azure_devops_1_0": [0],
        "ex_ci_cd_1_0": [0],
        "ex_cloud_1_0": [0],
        "ex_outras_clouds_1_0": [0],
        "ex_database_1_0": [0],
        "ex_outros_database_1_0": [0],
        "ex_tdd_1_0": [0],
        "ex_unit_test_1_0": [0],
        "ex_integration_test_1_0": [0],
        "ex_scrum_1_0": [0],
        "ex_kanban_1_0": [0],
        "atuou_projeto_ai_1_0": [0],
        "domina_ai_1_0": [0],
        "regime_contratacao_1_0": [0],
    }
)


new_offer_prediction = model.predict(new_candidate)
print(f"Oferta ideal para o candidato: {new_offer_prediction[0]:.2f}")


# Plotar valores reais vs valores previstos
y_pred_all = model.predict(X_test)

# Calcular a linha de regressão linear
x_range = np.linspace(min(y_test), max(y_test), 100)
y_range = x_range
line_trace = go.Scatter(
    x=x_range,
    y=y_range,
    mode="lines",
    name="Linha de Regressão Linear",
    line=dict(color="pink", width=3),
)

# Plotar valores reais vs valores previstos
trace_real_vs_pred = go.Scatter(
    x=y_test,
    y=y_pred_all,
    mode="markers",
    marker=dict(color="blue"),
    name="Valores Reais vs Valores Previstos",
)

# Adicionar o valor de new_offer_prediction no gráfico em vermelho
trace_new_offer = go.Scatter(
    x=[new_offer_prediction[0]],
    y=[new_offer_prediction[0]],
    mode="markers",
    marker=dict(color="red", size=10),
    name="Nova Oferta Prevista",
)

layout = go.Layout(
    title="Valores Reais vs Valores Previstos",
    xaxis=dict(title="Valores Reais"),
    yaxis=dict(title="Valores Previstos"),
)

fig = go.Figure(data=[trace_real_vs_pred, trace_new_offer, line_trace], layout=layout)
fig.show()
