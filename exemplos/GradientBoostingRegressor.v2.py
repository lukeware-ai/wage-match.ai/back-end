import joblib
import pandas as pd

model = joblib.load("exemplos/modelo-v6.pkl")

# Fazer previsões com novos dados (exemplo)
# new_candidate = pd.DataFrame(
#     {
#         "ano_atual": [2023.00, 2023.00, 2024.00],
#         "area": ["desenvilvomento_de_software", "desenvilvomento_de_software", "desenvilvomento_de_software"],
#         "cargo": ["desenvolvedor_back_end", "desenvolvedor_back_end", "desenvolvedor_back_end"],
#         "estado": ["sao_paulo", "sao_paulo", "sao_paulo"],
#         "ex_liguagem_desenv": ["python", "python", "java"],
#         "nivel": [0, 3, 1],
#         "demanda": [0,0,0],
#         "ex_framework_1_0": [0,0,0],
#         "ex_outros_frameworks_1_0": [0,0,0],
#         "ex_idea_1_0": [0,0,0],
#         "ex_outras_ideas_1_0": [0,0,0],
#         "ex_git_1_0": [0,0,0],
#         "ex_jira_1_0": [0,0,0],
#         "ex_confluence_1_0": [0,0,0],
#         "ex_gitlab_1_0": [0,0,0],
#         "ex_github_1_0": [0,0,0],
#         "ex_azure_devops_1_0": [0,0,0],
#         "ex_ci_cd_1_0": [0,0,0],
#         "ex_cloud_1_0": [0,0,0],
#         "ex_outras_clouds_1_0": [0,0,0],
#         "ex_database_1_0": [0,0,0],
#         "ex_outros_database_1_0": [0,0,0],
#         "ex_tdd_1_0": [0,0,0],
#         "ex_unit_test_1_0": [0,0,0],
#         "ex_integration_test_1_0": [0,0,0],
#         "ex_scrum_1_0": [0,0,0],
#         "ex_kanban_1_0": [0,0,0],
#         "atuou_projeto_ai_1_0": [0,0,0],
#         "domina_ai_1_0": [0,0,0],
#         "regime_contratacao_1_0": [0,0,0],
#     }
# )
new_instance = pd.DataFrame(
    {
        "ano_atual": [2023],
        "area": ["desenvolvimento_de_software"],
        "cargo": ["desenvolvedor_back_end"],
        "estado": ["sao_paulo"],
        "ex_liguagem_desenv": ["python"],
        "nivel": [2],
        "demanda": [0],
        "ex_framework_1_0": [0],
        "ex_outros_frameworks_1_0": [0],
        "ex_idea_1_0": [0],
        "ex_outras_ideas_1_0": [0],
        "ex_git_1_0": [0],
        "ex_jira_1_0": [0],
        "ex_confluence_1_0": [0],
        "ex_gitlab_1_0": [0],
        "ex_github_1_0": [0],
        "ex_azure_devops_1_0": [0],
        "ex_ci_cd_1_0": [0],
        "ex_cloud_1_0": [0],
        "ex_outras_clouds_1_0": [0],
        "ex_database_1_0": [0],
        "ex_outros_database_1_0": [0],
        "ex_tdd_1_0": [0],
        "ex_unit_test_1_0": [0],
        "ex_integration_test_1_0": [0],
        "ex_scrum_1_0": [0],
        "ex_kanban_1_0": [0],
        "atuou_projeto_ai_1_0": [0],
        "domina_ai_1_0": [0],
        "regime_contratacao_1_0": [0],
    }
)

new_offer_prediction = model.predict(new_instance)
print(f"Oferta ideal para o candidato: {new_offer_prediction[0]:.2f}")



# Carregar os dados do CSV
df = pd.read_csv('exemplos/oferta-back-end-python.csv')


# DataFrames vazios para os dados conhecidos e desconhecidos
new_candidate_known = pd.DataFrame(columns=new_instance.columns)
new_candidate_unknown = pd.DataFrame(columns=new_instance.columns)

# Verificar se os valores de uma linha estão presentes nos dados conhecidos
def is_known(row):
    return all(row[column] in df[column].unique() for column in new_instance.columns if column in df.columns)

# Separar os dados conhecidos e desconhecidos
for _, row in new_instance.iterrows():
    if is_known(row):
        new_candidate_known = pd.concat([new_candidate_known, pd.DataFrame([row])], ignore_index=True)
    else:
        new_candidate_unknown = pd.concat([new_candidate_unknown, pd.DataFrame([row])], ignore_index=True)

# Exibir os DataFrames
if len(new_candidate_known.head()) > 0:
    print("Dados conhecidos:")
    print(new_candidate_known.head)


if len(new_candidate_unknown.head()) > 0:
    print("\nDados desconhecidos:")
    print(new_candidate_unknown.head)


# # Inicializar DataFrames vazios para os dados conhecidos e desconhecidos
# new_candidate_known = pd.DataFrame(columns=new_candidate.columns)
# new_candidate_unknown = pd.DataFrame(columns=new_candidate.columns)

# # Iterar sobre as linhas do new_candidate
# for index, row in new_candidate.iterrows():
#     # Definir uma flag para indicar se a linha contém dados desconhecidos
#     unknown_flag = False
#     # Iterar sobre as colunas do new_candidate
#     for column in new_candidate.columns:
#         # Verificar se a coluna está presente no DataFrame df (dados do CSV)
#         if column in df.columns:
#             # Verificar se o valor da coluna está presente no DataFrame df
#             if row[column] not in df[column].unique():
#                 unknown_flag = True
#                 break  # Parar a verificação se algum valor for desconhecido
#     # Adicionar a linha ao DataFrame correspondente
#     if unknown_flag:
#         new_candidate_unknown = pd.concat([new_candidate_unknown, pd.DataFrame([row])], ignore_index=True)
#     else:
#         new_candidate_known = pd.concat([new_candidate_known, pd.DataFrame([row])], ignore_index=True)

# # Exibir o DataFrame new_candidate_known
# print("Dados conhecidos:")
# print(new_candidate_known)

# # Exibir o DataFrame new_candidate_unknown
# print("\nDados desconhecidos:")
# print(new_candidate_unknown)

